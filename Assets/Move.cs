﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Move : MonoBehaviour
{
    private float timeEncounter = 0;
    private Rigidbody2D body;
    private float speed;
    private float width;
    private float height;

    // Start is called before the first frame update
    void Start()
    {
        speed = 2;
        width = 2;
        height = 2;
        body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        timeEncounter += Time.deltaTime * speed;
        float x = Mathf.Cos(timeEncounter) * width;
        float y = Mathf.Sin(timeEncounter) * height;
        float z = 10;
        body.velocity = new Vector3(x, y, z);
        //transform.position = new Vector3(x,y,z);
    }
}
